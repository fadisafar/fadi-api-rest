package main

import (
	"fmt"
	"log"
	"os"
	"strconv"
    _ "net"
	"PROJECTGO1/api/handlers"
	"PROJECTGO1/api/routes"
	"PROJECTGO1/api/utils"
	"PROJECTGO1/config"
	dbm "PROJECTGO1/db/dbmodels"
	_ "PROJECTGO1/docs"

	"github.com/joho/godotenv"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	echoSwagger "github.com/swaggo/echo-swagger"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

// @title PROJECTGO1 API
// @description This is the API documentation for PROJECTGO1
// @host localhost:8080
// @BasePath /
// @schemes http

type Configuration struct {
    Server ServerConfiguration
    DB     DBConfiguration
    JWT    config.Config
}

type ServerConfiguration struct {
	Host string
	Port int
}

type DBConfiguration struct {
	dsn string
}

type JWTConfiguration struct {
	Secret string
}

func ParseEnvInt(envVar string, defaultValue int) int {
	value, exists := os.LookupEnv(envVar)
	if !exists {
		return defaultValue
	}

	parsedValue, err := strconv.Atoi(value)
	if err != nil {
		log.Fatalf("failed to parse environment variable %s: %v", envVar, err)
	}

	return parsedValue
}

func LoadConfig() (*Configuration, error) {
	err := godotenv.Load()
	if err != nil {
		return nil, fmt.Errorf("failed to load .env file: %w", err)
	}

	serverConfig := ServerConfiguration{
		Host: os.Getenv("SERVER_HOST"),
		Port: ParseEnvInt("SERVER_PORT", 8080),
	}

	dbConfig := DBConfiguration{
		dsn: fmt.Sprintf(
			"host=%s port=%d user=%s password=%s dbname=%s sslmode=disable",
			os.Getenv("DB_HOST"),
			ParseEnvInt("DB_PORT", 5432),
			os.Getenv("DB_USER"),
			os.Getenv("DB_PASSWORD"),
			os.Getenv("DB_NAME"),
		),
	}


cfg, err := config.Load()
if err != nil {
    log.Fatalf("failed to load config: %v", err)
}

jwtConfig := config.Config{
    JWT: struct {
        SecretKey []byte
    }{
        SecretKey: cfg.JWT.SecretKey,
    },
}

    return &Configuration{
        Server: serverConfig,
        DB:     dbConfig,
        JWT:    jwtConfig,
	}, nil

}

func main() {
	// Load the configuration from the .env file
	cfg, err := LoadConfig()
	if err != nil {
		log.Fatalf("failed to load config: %v", err)
	}

	// Open a database connection
	db, err := gorm.Open(postgres.Open(cfg.DB.dsn), &gorm.Config{})
	if err != nil {
		log.Fatalf("failed to open database connection: %v", err)
	}

	// Close the database connection when the main function exits
	sqlDB, err := db.DB()
	if err != nil {
		log.Fatalf("failed to get underlying sql.DB: %v", err)
	}
	defer func() {
		if err := sqlDB.Close(); err != nil {
			log.Fatalf("failed to close database connection: %v", err)
		}
	}()

	// Migrate the database schema
	err = db.AutoMigrate(&dbm.User{}, &dbm.Role{}, &dbm.AuthToken{}, &dbm.RefreshToken{}, &dbm.Group{})
	if err != nil {
		log.Fatalf("failed to migrate database schema: %v", err)
	}

	// Initialize the Echo router
	e := echo.New()

	   // define other endpoints...
	// Register middleware
	e.Use(middleware.Logger())
	e.Use(utils.RequestLoggerMiddleware)

	e.Use(middleware.CORSWithConfig(middleware.CORSConfig{
	AllowOrigins: []string{"*"},
	AllowMethods: []string{echo.GET, echo.POST, echo.PUT, echo.DELETE, echo.OPTIONS},
	AllowHeaders: []string{echo.HeaderContentType},
	}))

		// Register routes
	authHandler := handlers.NewAuthHandler(db, &cfg.JWT)
	routes.RegisterAuthRoutes(e, authHandler, db)
	routes.RegisterUserRoutes(e, handlers.NewUserHandler(db))
	routes.RegisterRoleRoutes(e, handlers.NewRoleHandler(db))
	routes.RegisterGroupRoutes(e, handlers.NewGroupHandler(db))
	routes.RegisterRefreshTokenRoutes(e)



	e.GET("/swagger/*", echoSwagger.WrapHandler)


	// Start the HTTP server
	addr := fmt.Sprintf("%s:%d", cfg.Server.Host, cfg.Server.Port)
	log.Printf("starting HTTP server on %s", addr)
	if err := e.Start(addr); err != nil {
		log.Fatalf("failed to start HTTP server: %v", err)
	}
}
