package config

import (
	"log"
	"gorm.io/gorm"
	"gorm.io/plugin/dbresolver"
)

var (
    dbc          *gorm.DB
    JwtSecretKey []byte
)

func init() {
    // Set JWT secret key
    JwtSecretKey = []byte("5aU5ztpNMxmt6gcO4/HyY15EtQHfvdwAwd/TQXEVdOY=")
    if len(JwtSecretKey) == 0 {
        log.Fatal("JWT_SECRET_KEY environment variable not set")
    }
}


// Config represents the application configuration
type Config struct {
    DB struct {
        dsn           string
        ConnMaxIdle   int
        ConnMaxOpen   int
        ConnMaxLife   int
        PreferSlave   bool
        ReadOnly      bool
        ShuffleRead   bool
        Resolver      *dbresolver.DBResolver
    }
    Server struct {
        Host string
        Port int
    }
    JWT struct {
        SecretKey []byte
    }
}


// Load loads the application configuration from the environment
func Load() (Config, error) {
    var cfg Config

    cfg.DB.dsn = "host=localhost user=postgres password=projectdevops dbname=apirest port=5432 sslmode=disable"
    cfg.Server.Host = "localhost"
    cfg.Server.Port = 8080
    cfg.JWT.SecretKey = []byte("5aU5ztpNMxmt6gcO4/HyY15EtQHfvdwAwd/TQXEVdOY=")

    return cfg, nil
}



// authTransport is an http.RoundTripper that adds the Authorization header to the request.


// type Client struct {
//     baseURL    *url.URL
//     httpClient *http.Client
// }
