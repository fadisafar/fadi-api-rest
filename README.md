REST API for User Management and Authentication
This API is built using Golang and PostgreSQL to manage users, roles, user groups, and user authentication based on JWT.


Endpoints
The API implements the following endpoints:



/users: used to manage users (GET, POST, PUT, DELETE).
/roles: allows managing roles for users (GET, POST, PUT, DELETE).
/groups: used to manage user groups (GET, POST, PUT, DELETE).
/auth: used to manage user authentication using JWT (POST).


Every endpoint is protected by JWT authentication, except the /auth endpoint.
Just the admin is authorized to do GET, POST, PUT, DELETE operation in database.
Admin login with email and password:

email: admin@gmail.com
password: admin1234

He get an access-token that will be used in Authorization field named in Swagger as <access-token> 
in every operation. 



Technical Stack:

Golang
PostgreSQL
Echo library for API implementation
Gorm library for database management
jwt-go library for JWT authentication
OpenAPI (Swagger) for API documentation



Object Attributes
The following are the attributes of the objects used in the database:

User: id, name, email, password, roles, groups, created_at, updated_at, deleted_at
Role: id, name, description, created_at, updated_at, deleted_at
Group: id, name, parent_group_id, child_group_ids, created_at, updated_at, deleted_at


API Documentation
Please refer to the openapi.yaml file in the root directory for detailed API documentation, including data models and response schemas, and a description of any errors that may be returned.

Testing the API
http://localhost:8080/swagger/index.html

