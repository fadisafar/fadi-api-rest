package dbmodels

import (
    "time"
    // "gorm.io/gorm"
)


type RefreshToken struct {
    // gorm.Model
    // The ID of the refresh token.
    // required: true
    ID         uint `gorm:"primaryKey"`
    // The token.
    // required: true
    // example: admin
    Token      string
    // The expiration date and time of the token.
    // required: false
    ExpiresAt  time.Time
    // The ID of the user.
    // required: true
    UserID     uint
    
    User       User
}


// CreateRefreshToken creates a new refresh token in the database.
// ---
// summary: Create a new refresh token.
// parameters:
// - name: token
//   in: query
//   description: The token string.
//   required: true
//   type: string
// - name: expiresAt
//   in: query
//   description: The expiration time of the token.
//   required: true
//   type: string
//   format: date-time
// - name: user
//   in: query
//   description: The user associated with the token.
//   required: true
//   type: object
//   schema:
//     "$ref": "#/definitions/User"
// responses:
//   '200':
//     description: The newly created refresh token.
//     schema:
//       "$ref": "#/definitions/RefreshToken"

func CreateRefreshToken(token string, expiresAt time.Time, user User) (*RefreshToken, error) {
    refreshToken := &RefreshToken{
        Token:     token,
        ExpiresAt: expiresAt,
        User:      user,
    }

    err := dbc.Create(refreshToken).Error
    if err != nil {
        return nil, err
    }

    return refreshToken, nil
}
