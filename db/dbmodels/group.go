package dbmodels

import (
	"time"
	// "gorm.io/gorm"
)

// var dbc *gorm.DB

type Group struct {
    // gorm.Model
    // The ID of the group.
    // required: true
	ID             uint           `gorm:"primaryKey"`
    // The name of the group.
    // required: true
	Name           string         `gorm:"not null"`
	ParentGroupID  uint           `gorm:"index"`
	ParentGroup    *Group         `gorm:"foreignKey:ParentGroupID"`
	ChildGroups    []*Group       `gorm:"foreignKey:ParentGroupID;constraint:OnDelete:CASCADE;"`
	Users          []*User        `gorm:"many2many:user_groups;constraint:OnDelete:CASCADE;"`
    // The creation date and time of the group.
    // required: true
    CreatedAt      time.Time
    // The last update date and time of the group.
    // required: true
	UpdatedAt      time.Time
    // The deletion date and time of the group.
    // required: false
	DeletedAt      time.Time
    // DeletedAt      gorm.DeletedAt `gorm:"index"`
}


// CreateGroup creates a new group with the given name and optional parent group.
// Returns the created group or an error.
// swagger:operation POST /groups CreateGroup
//
// Creates a new group in the system.
//
// ---
// produces:
// - application/json
// parameters:
// - name: group
//   in: body
//   description: The group to create.
//   required: true
//   schema:
//     "$ref": "#/definitions/GroupInput"
// responses:
//   '200':
//     description: The created group.
//     schema:
//       "$ref": "#/definitions/Group"
//   '400':
//     description: Invalid input.
//   '500':
//     description: Internal server error.
func CreateGroup(name string, parentGroup *Group) (*Group, error) {
    group := &Group{
        Name:          name,
        // ParentGroup:   parentGroup,
    }

    err := dbc.Create(group).Error
    if err != nil {
        return nil, err
    }

    return group, nil
}

// AddChildGroup adds the given group as a child group to this group.
// Returns an error if the operation fails.
// swagger:operation POST /groups/{group_id}/child-groups AddChildGroup
//
// Adds the given group as a child group to this group.
//
// ---
// produces:
// - application/json
// parameters:
// - name: group_id
//   in: path
//   description: The ID of the parent group.
//   required: true
//   type: integer
// - name: group
//   in: body
//   description: The child group to add.
//   required: true
//   schema:
//     "$ref": "#/definitions/GroupInput"
// responses:
//   '200':
//     description: Child group added successfully.
//   '400':
//     description: Invalid input.
//   '500':
//     description: Internal server error.
func (group *Group) AddChildGroup(childGroup *Group) error {
    err := dbc.Model(group).Association("ChildGroups").Append(childGroup)
    return err
}
// AddUser adds the given user to this group.
// Returns an error if the operation fails.
// swagger:operation POST /groups/{group_id}/users AddUser
//
// Adds the given user to this group.
//
// ---
// produces:
// - application/json
// parameters:
// - name: group_id
//   in: path
//   description: The ID of the group.
//   required: true
//   type: integer
// - name: user
//   in: body
//   description: The user to add.
//   required: true
//   schema:
//     "$ref": "#/definitions/UserInput"
// responses:
//   '200':
//     description: User added successfully.
//   '400':
//     description: Invalid input.
//   '500':
//     description: Internal server error.
func (group *Group) AddUser(user *User) error {
    err := dbc.Model(group).Association("Users").Append(user)
    return err
}


