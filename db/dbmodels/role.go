package dbmodels

import (
    "errors"
    "time"
    "gorm.io/gorm"
)

type Role struct {
    // gorm.Model
    // The ID of the role.
    // required: true
    ID          uint `gorm:"primaryKey"`
    // The name of the role.
    // required: true
    // example: admin
    Name        string
    // The description of the role.
    // example: This is the admin role.
    Description string
    // The users associated with the role.
    Users       []*User `gorm:"many2many:user_roles;"`
    // The creation date and time of the role.
    // required: true
    CreatedAt   time.Time
    // The last update date and time of the role.
    // required: true
    UpdatedAt   time.Time
    // The deletion date and time of the role.
    // required: false
    DeletedAt   time.Time
}



func (r *Role) TableName() string {
    return "roles"
}

// swagger:operation POST /roles createRole
//
// Creates a new role.
//
// ---
// produces:
// - application/json
// parameters:
// - in: body
//   name: role
//   description: The role to create.
//   schema:
//     type: object
//     required:
//     - name
//     properties:
//       name:
//         type: string
//         description: The name of the role.
//       description:
//         type: string
//         description: The description of the role.
// responses:
//   '200':
//     description: OK
//     schema:
//       $ref: "#/definitions/Role"
//   '400':
//     description: Bad request
//   '500':
//     description: Internal server error

func CreateRole(name string, description string) (*Role, error) {
    if name == "" {
        return nil, errors.New("role name cannot be empty")
    }

    // Check if a role with the same name already exists in the database
    var existingRole Role
    err := dbc.Where("name = ?", name).First(&existingRole).Error
    if err != nil && !errors.Is(err, gorm.ErrRecordNotFound) {
        return nil, err
    }
    if existingRole.ID != 0 {
        return nil, errors.New("a role with the same name already exists")
    }

    role := &Role{
        Name:        name,
        Description: description,
    }

    err = dbc.Create(role).Error
    if err != nil {
        return nil, err
    }

    return role, nil
}

// swagger:operation PUT /roles/{id}/users/{user_id} addRoleUser
//
// Adds a user to a role.
//
// ---
// produces:
// - application/json
// parameters:
// - in: path
//   name: id
//   description: The ID of the role.
//   type: integer
//   required: true
// - in: path
//   name: user_id
//   description: The ID of the user.
//   type: integer
//   required: true
// responses:
//   '200':
//     description: OK
//   '400':
//     description: Bad request
//   '500':
//     description: Internal server error

func (role *Role) AddUser(user *User) error {
    err := dbc.Model(role).Association("Users").Append(user)
    return err
}


func FindRoleByName(dbc *gorm.DB, name string) (Role, error) {
    var role Role
    result := dbc.Where("name = ?", name).First(&role)
    if result.Error != nil {
        return Role{}, result.Error
    }
    return role, nil
}

