package dbmodels

import (
	"time"
	"gorm.io/gorm"
	// "gorm.io/gorm"
)

// swagger:model User
type User struct {
    // gorm.Model
    // The ID of the user.
    // required: true
    ID        uint   `gorm:"primaryKey"`
    // The name of the user.
    // required: true
    // example: John Doe
    Name      string `gorm:"not null"`
    // The email of the user.
    // required: true
    // example: john.doe@example.com
    Email     *string `gorm:"unique;not null"`
    // The password of the user.
    // required: true
    Password  string `gorm:"not null"`
    // The roles of the user.
    Roles     []*Role `gorm:"many2many:user_roles;constraint:OnDelete:CASCADE;"`
    // The groups of the user.
    Groups    []*Group `gorm:"many2many:user_groups;constraint:OnDelete:CASCADE;"`
    // The creation date and time of the user.
    // required: true
    CreatedAt time.Time
    // The last update date and time of the user.
    // required: true
    UpdatedAt time.Time
    // The deletion date and time of the user.
    // required: false
    DeletedAt time.Time
    // DeletedAt gorm.DeletedAt `gorm:"index"`
}

type UpdateUser struct {
    Name     *string `json:"name"`
    Email    *string `json:"email"`
    Password *string `json:"password"`
}


// swagger:operation GET /users/{email} getUserByEmail
//
// Gets the user with the specified email address.
//
// ---
// produces:
// - application/json
// parameters:
// - name: email
//   in: path
//   description: The email address of the user to get.
//   required: true
//   type: string
// responses:
//   '200':
//     description: OK
//     schema:
//       $ref: "#/definitions/User"
//   '400':
//     description: Bad request
//   '401':
//     description: Unauthorized
//   '404':
//     description: Not found
//   '500':
//     description: Internal server error
//

func GetUserByEmail(dbc *gorm.DB, email string) (User, error) {
    var user User
    result := dbc.Where("email = ?", email).Preload("Roles").First(&user)
    if result.Error != nil {
        return User{}, result.Error
    }
    return user, nil
}

// swagger:operation GET /users/{id} getUserById
//
// Gets a user by ID.
//
// ---
// produces:
// - application/json
// parameters:
// - name: id
//   in: path
//   description: The ID of the user to get.
//   required: true
//   type: integer
// responses:
//   '200':
//     description: OK
//     schema:
//       $ref: "#/definitions/User"
//   '404':
//     description: User not found
//   '500':
//     description: Internal server error
//
// func GetUserById(id uint) (User, error) {
//     var user User
//     result := dbc.First(&user, id)
//     if result.Error != nil {
//         return User{}, result.Error
//     }
//     return user, nil
// }

func GetUserById(dbc *gorm.DB, id uint) (User, error) {
    var user User
    result := dbc.First(&user, id)
    if result.Error != nil {
        return User{}, result.Error
    }
    return user, nil
}

