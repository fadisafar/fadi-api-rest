

package db

import (
	"fmt"
	"log"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	dbm "PROJECTGO1/db/dbmodels"
	"os"

)

var dbc *gorm.DB
// InitDatabase initializes a connection to the database.
//
// swagger:operation POST /database/initDatabase database initDatabase
//
// ---
// summary: Initializes a connection to the database.
// responses:
//   '200':
//     description: Successfully initialized a connection to the database.
//   '500':
//     description: Failed to connect to the database.
func InitDatabase() {
	dsn := fmt.Sprintf("host=%s user=%s password=%s dbname=%s port=%s sslmode=disable TimeZone=Asia/Shanghai",
		os.Getenv("DB_HOST"),
		os.Getenv("DB_USER"),
		os.Getenv("DB_PASSWORD"),
		os.Getenv("DB_NAME"),
		os.Getenv("DB_PORT"),
	)

	var err error
	dbc, err = gorm.Open(postgres.Open(dsn), &gorm.Config{})
	if err != nil {
		log.Fatalf("Failed to connect to database: %s", err)
	}
}


// MigrateDatabase migrates the database schema.
//
// swagger:operation POST /database/migrateDatabase database migrateDatabase
//
// ---
// summary: Migrates the database schema.
// responses:
//   '200':
//     description: Successfully migrated the database schema.
//   '500':
//     description: Failed to migrate the database schema.
func MigrateDatabase() {
	err := dbc.AutoMigrate(&dbm.User{}, &dbm.Role{}, &dbm.AuthToken{}, &dbm.RefreshToken{}, &dbm.Group{})
	if err != nil {
		log.Fatalf("Failed to migrate database: %s", err)
	}

}
// Open opens a connection to the database.
//
// swagger:operation POST /database/open database openDatabaseConnection
//
// ---
// summary: Opens a connection to the database.
// parameters:
// - in: query
//   name: dsn
//   description: The data source name (DSN) of the database.
//   required: true
//   type: string
// responses:
//   '200':
//     description: Successfully opened a connection to the database.
//   '500':
//     description: Failed to open a connection to the database.
func Open(dsn string) (*gorm.DB, error) {
    return gorm.Open(postgres.Open(dsn), &gorm.Config{})
}

func GetDB() *gorm.DB {
    return dbc
}
