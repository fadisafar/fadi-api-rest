package migrations

import (
    "gorm.io/gorm"
)

func ChangeChildGroupIdsType(tx *gorm.DB) error {
    // Change the data type of the child_group_ids column to "bigint[]"
    err := tx.Exec("ALTER TABLE groups ALTER COLUMN child_group_ids TYPE bigint[] USING child_group_ids::bigint[]").Error
    if err != nil {
        return err
    }

    return nil
}