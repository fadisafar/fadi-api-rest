package utils

import (
	"errors"
	"regexp"
	"strings"
)


func ValidateEmail(email string) error {
	email = strings.TrimSpace(email)
	if email == "" {
		return errors.New("Email is required")
	}

	if !regexp.MustCompile(`^[^\s@]+@[^\s@]+\.[^\s@]+$`).MatchString(email) {
		return errors.New("Invalid email format")
	}

	return nil
}


func ValidatePassword(password string) error {
	password = strings.TrimSpace(password)
	if password == "" {
		return errors.New("Password is required")
	}

	if len(password) < 8 {
		return errors.New("Password should be at least 8 characters long")
	}

	return nil
}
