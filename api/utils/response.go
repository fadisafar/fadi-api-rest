package utils

import (
    "log"
    "net/http"
    "github.com/labstack/echo/v4"
)

// Response represents a response structure.
type Response struct {
    Success bool        `json:"success"`
    Data    interface{} `json:"data"`
    Error   string      `json:"error,omitempty"`
}


func JSON(c echo.Context, statusCode int, data interface{}, err error) error {
    var response Response

    if err != nil {
        response = Response{
            Success: false,
            Data:    nil,
            Error:   err.Error(),
        }
    } else {
        response = Response{
            Success: true,
            Data:    data,
            Error:   "",
        }
    }

    return c.JSON(statusCode, response)
}


func Error(c echo.Context, statusCode int, errMsg string) error {
    response := Response{
        Success: false,
        Data:    nil,
        Error:   errMsg,
    }
    return c.JSON(statusCode, response)
}

func Success(c echo.Context, data interface{}) error {
    response := Response{
        Success: true,
        Data:    data,
        Error:   "",
    }
    return c.JSON(http.StatusOK, response)
}


func RequestLoggerMiddleware(next echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {
		req := c.Request()
		res := c.Response()

		if err := next(c); err != nil {
			c.Error(err)
		}

		log.Printf("%s %s %d %s", req.Method, req.URL.Path, res.Status, res.Header().Get("Content-Type"))

		return nil
	}
}