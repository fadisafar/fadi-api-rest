package dtos

type RoleDto struct {
    ID          uint   `json:"id"`
    Name        string `json:"name"`
    Description string `json:"description"`
}

type NewRoleDto struct {
    Name        string `json:"name"`
    Description string `json:"description"`
}

type UpdatedRoleDto struct {
    Name        string `json:"name"`
    Description string `json:"description"`
}