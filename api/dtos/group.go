package dtos

type GroupDto struct {
    ID             uint   `json:"id"`
    Name           string
    ParentGroupID  uint
    ChildGroupsIDs []uint
}

type NewGroupDto struct {
    Name           string
    ParentGroupID  uint
    ChildGroupsIDs []uint
}

type UpdatedGroupDto struct {
    Name           string
    ParentGroupID  uint
    ChildGroupsIDs []uint
}



