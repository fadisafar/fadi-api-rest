package dtos


type UserDto struct {
	FullName string 
	Email    *string
	Password  string
    RoleIDs  []uint   `json:"role_ids"`
    GroupIDs []uint   `json:"group_ids"`
   }


