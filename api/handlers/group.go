

package handlers

import (
	"net/http"
	"strconv"
	"PROJECTGO1/db/dbmodels"
	"PROJECTGO1/api/dtos"
	"github.com/labstack/echo/v4"
	"gorm.io/gorm"
	"time"

)

// NewGroupHandler initializes new GroupHandler
func NewGroupHandler(db *gorm.DB) *Handler {
	return &Handler{ DB: db,}
}
// @Summary Get all groups
// @Tags groups
// @Produce json
// @Param Authorization header string true "Access token"
// @Success 200 {array} dtos.GroupDto
// @Router /groups [get]
func (gh *Handler) GetGroups(c echo.Context) error {
	var groups []dbmodels.Group

	if err := gh.DB.Preload("ChildGroups").Preload("ParentGroup").Find(&groups).Error; err != nil {
		return c.JSON(http.StatusInternalServerError, err.Error())
	}

	// Create a new slice to hold the transformed data
	dtoGroups := make([]dtos.GroupDto, 0, len(groups))

	// Convert each dbmodels.Group object to a dtos.GroupDto object
	for _, group := range groups {
		dtoGroup := dtos.GroupDto{
			ID:   group.ID, // add ID to dtoGroup
			Name: group.Name,
		}

		// Add the parent group ID to the dtoGroup if it exists
		if group.ParentGroup != nil {
			dtoGroup.ParentGroupID = group.ParentGroup.ID
		}

		// Add the child group IDs to the dtoGroup if they exist
		if len(group.ChildGroups) > 0 {
			childGroupIDs := make([]uint, len(group.ChildGroups))
			for i, childGroup := range group.ChildGroups {
				childGroupIDs[i] = childGroup.ID
			}
			dtoGroup.ChildGroupsIDs = childGroupIDs
		}

		dtoGroups = append(dtoGroups, dtoGroup)
	}

	return c.JSON(http.StatusOK, dtoGroups)
}

// @Summary Get a group
// @Tags groups
// @Produce json
// @Param Authorization header string true "Access token"
// @Param id path int true "Group ID"
// @Success 200 {object} dbmodels.Group
// @Success 404 {string} string "group not found"
// @Router /groups/{id} [get]
func (gh *Handler) GetGroup(c echo.Context) error {
	groupID, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		return c.JSON(http.StatusBadRequest, err.Error())
	}

	var group dbmodels.Group
	if err := gh.DB.Preload("ChildGroups").First(&group, groupID).Error; err != nil {
		if err == gorm.ErrRecordNotFound {
			return c.JSON(http.StatusNotFound, "group not found")
		}
		return c.JSON(http.StatusInternalServerError, err.Error())
	}

	return c.JSON(http.StatusOK, group)
}


// CreateGroup handles POST /groups route
// @Summary Create a group
// @Tags groups
// @Accept json
// @Produce json
// @Param Authorization header string true "Access token"
// @Param group body dtos.NewGroupDto true "Group Object"
// @Success 201 {object} dbmodels.Group
// @Router /groups [post]
func (gh *Handler) CreateGroup(c echo.Context) error {
    groupDto := new(dtos.NewGroupDto)
    if err := c.Bind(groupDto); err != nil {
        return c.JSON(http.StatusBadRequest, err.Error())
    }

    var parentGroup dbmodels.Group
    if err := gh.DB.First(&parentGroup, groupDto.ParentGroupID).Error; err != nil {
        return c.JSON(http.StatusInternalServerError, err.Error())
    }

    childGroups := make([]*dbmodels.Group, 0, len(groupDto.ChildGroupsIDs))
    for _, childID := range groupDto.ChildGroupsIDs {
        var childGroup dbmodels.Group
        if err := gh.DB.First(&childGroup, childID).Error; err != nil {
            return c.JSON(http.StatusInternalServerError, err.Error())
        }
        childGroups = append(childGroups, &childGroup)
    }

    groupInfo := &dbmodels.Group{
        CreatedAt:   time.Now(),
        Name:        groupDto.Name,
        ParentGroup: &parentGroup,
        ChildGroups: childGroups,
    }

    if err := gh.DB.Create(groupInfo).Error; err != nil {
        return c.JSON(http.StatusInternalServerError, err.Error())
    }

    return c.JSON(http.StatusCreated, groupInfo)
}

// DeleteGroup handles DELETE /groups/:id route
// @Summary Delete a group
// @Tags groups
// @Produce json
// @Param Authorization header string true "Access token"
// @Param id path int true "Group ID"
// @Success 200 "Group deleted successfully"
// @Success 404 {string} string "group not found"
// @Router /groups/{id} [delete]
func (gh *Handler) DeleteGroup(c echo.Context) error {
	groupID, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		return c.JSON(http.StatusBadRequest, err.Error())
	}

	group := new(dbmodels.Group)
	if err := gh.DB.Preload("ChildGroups").First(group, groupID).Error; err != nil {
		if err == gorm.ErrRecordNotFound {
			return c.JSON(http.StatusNotFound, "group not found")
		}
		return c.JSON(http.StatusInternalServerError, err.Error())
	}

	if len(group.ChildGroups) > 0 {
		for _, childGroup := range group.ChildGroups {
			if err := gh.DB.Delete(&dbmodels.Group{}, childGroup.ID).Error; err != nil {
				return c.JSON(http.StatusInternalServerError, err.Error())
			}
		}
	}

	if err := gh.DB.Delete(group).Error; err != nil {
		if err == gorm.ErrRecordNotFound {
			return c.JSON(http.StatusNotFound, "group not found")
		}
		return c.JSON(http.StatusInternalServerError, err.Error())
	}

	return c.JSON(http.StatusOK, map[string]string{
		"message": "Group deleted successfully",
	})
}


// UpdateGroup handles PUT /groups/:id route
// @Summary Update a group
// @Tags groups
// @Accept json
// @Produce json
// @Param Authorization header string true "Access token"
// @Param id path int true "Group ID"
// @Param group body dtos.UpdatedGroupDto true "Group Object"
// @Success 200 "Group updated successfully"
// @Success 404 {string} string "group not found"
// @Router /groups/{id} [put]
func (gh *Handler) UpdateGroup(c echo.Context) error {
	groupID, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		return c.JSON(http.StatusBadRequest, err.Error())
	}

	dtoGroup := new(dtos.UpdatedGroupDto)
	if err := c.Bind(dtoGroup); err != nil {
		return c.JSON(http.StatusBadRequest, err.Error())
	}

	group := new(dbmodels.Group)
	if err := gh.DB.Preload("ChildGroups").First(group, groupID).Error; err != nil {
		if err == gorm.ErrRecordNotFound {
			return c.JSON(http.StatusNotFound, "group not found")
		}
		return c.JSON(http.StatusInternalServerError, err.Error())
	}

	group.Name = dtoGroup.Name

	if dtoGroup.ParentGroupID != 0 {
		parentGroup := new(dbmodels.Group)
		if err := gh.DB.First(parentGroup, dtoGroup.ParentGroupID).Error; err != nil {
			return c.JSON(http.StatusInternalServerError, err.Error())
		}
		group.ParentGroup = parentGroup
	}

	if dtoGroup.ChildGroupsIDs != nil {
		childGroups := make([]*dbmodels.Group, len(dtoGroup.ChildGroupsIDs))
		for i, id := range dtoGroup.ChildGroupsIDs {
			childGroup := new(dbmodels.Group)
			if err := gh.DB.First(childGroup, id).Error; err != nil {
				return c.JSON(http.StatusInternalServerError, err.Error())
			}
			childGroups[i] = childGroup
		}
		group.ChildGroups = childGroups
	}

	if err := gh.DB.Save(group).Error; err != nil {
		return c.JSON(http.StatusInternalServerError, err.Error())
	}

	return c.JSON(http.StatusOK, map[string]string{
		"message": "Group updated successfully",
	})
}


