package handlers

import (
	"PROJECTGO1/api/utils"
	"PROJECTGO1/config"
	"PROJECTGO1/db/dbmodels"
	"fmt"
	"net/http"
	"time"
	"github.com/dgrijalva/jwt-go"
	"github.com/labstack/echo/v4"
	"golang.org/x/crypto/bcrypt"
	"gorm.io/gorm"
)


type AuthHandler struct {
    db *gorm.DB
    cfg *config.Config
}

func NewAuthHandler(db *gorm.DB, cfg *config.Config) *AuthHandler {
    return &AuthHandler{
        db: db,
        cfg: cfg,
    }
}


type AuthTokenResponse struct {
	Token       string `json:"token"`

}

type JWTClaim struct {
	UserId uint `json:"userId"`
	IsAdmin    bool `json:"isAdmin"`
	jwt.StandardClaims
}

// Login is the handler function for the login endpoint.
// @Summary Login
// @Description Login with username and password
// @Tags auth
// @Accept x-www-form-urlencoded
// @Produce json
// @Param email formData string true "Email"
// @Param password formData string true "Password"
// @Success 200 {object} AuthTokenResponse
// @Failure 401 {string} string "Unauthorized"
// @Router /auth/login [post]
func Login(c echo.Context, dbc *gorm.DB) error {
	email := c.FormValue("email")
	password := c.FormValue("password")

	// Validate the email and password inputs
	if err := utils.ValidateEmail(email); err != nil {
		return c.JSON(http.StatusBadRequest, map[string]string{"error": err.Error()})
	}

	if err := utils.ValidatePassword(password); err != nil {
		return c.JSON(http.StatusBadRequest, map[string]string{"error": err.Error()})
	}

	// Check if user exists
	user, err := dbmodels.GetUserByEmail(dbc, email)
	if err != nil {
		return echo.ErrUnauthorized
	}

	fmt.Println("--user roles--" ,user.Roles)

	// Compare the password with the hashed password stored in the database
	err = bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(password))
	if err != nil {
		return echo.ErrUnauthorized
	}

    isAdmin := false
    for _, role := range user.Roles {
            if role.Name == "Admin" {
                isAdmin = true
                break
            }
    }

	token, err:= GenerateTokenString(user.ID, isAdmin)

	response := &AuthTokenResponse{
		Token:        token,
	}

	return c.JSON(http.StatusOK, response)
}


func GenerateTokenString(userID uint,isAdmin bool) (string, error) {
    // Create a new token object
	expirationTime := time.Now().Add(1 * time.Hour)
	claims:= &JWTClaim{
		UserId: userID,
		IsAdmin: isAdmin,
		StandardClaims: jwt.StandardClaims{
			ExpiresAt: expirationTime.Unix(),
		},
	}
    token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)

    // Generate the signed token string
    tokenString, err := token.SignedString([]byte(config.JwtSecretKey))
    if err != nil {
        return "", err
    }

    return tokenString, nil
}


func ValidateToken(tokenString string) (error, bool) {
    // Parse the access token claims without validating
    token, err := jwt.ParseWithClaims(
        tokenString,
        &JWTClaim{},
        func(token *jwt.Token) (interface{}, error) {
            return []byte(config.JwtSecretKey), nil
        },
    )
    if err != nil {
        fmt.Println("Error parsing token:", err)
        return err, false
    }

    // Extract the isAdmin value from the access token's claims
    claims, ok := token.Claims.(*JWTClaim)
    if !ok {
        return fmt.Errorf("invalid token claims"), false
    }
    isAdmin := claims.IsAdmin

    return nil, isAdmin
}


func CheckAdmin() echo.MiddlewareFunc {
    return func(next echo.HandlerFunc) echo.HandlerFunc {
        return func(c echo.Context) error {
			
			

			tokenString := c.Request().Header.Get("Authorization")
			if tokenString == "" {
				return c.JSON(401,  "request does not contain an access token")
				
			}

			err,isAdmin:=ValidateToken(tokenString)

			if err != nil {
				return c.JSON(401,  "request does not contain a valid access token")
			}

			if(!isAdmin) {
				return c.JSON(401,  "User unauthorized to do this operation")
			}
            
			return next(c)
			
		}}}


