package handlers

import (
	"PROJECTGO1/api/dtos"
	"PROJECTGO1/db/dbmodels"
	"fmt"
	"net/http"
	"strconv"
	"time"

	"github.com/labstack/echo/v4"
	"golang.org/x/crypto/bcrypt"
	"gorm.io/gorm"
)

// Handler struct holds the DB instance
type Handler struct {
	DB *gorm.DB
}

// NewUserHandler returns a new instance of the User Handler
func NewUserHandler(db *gorm.DB) *Handler {
    return &Handler{
        DB: db,
    }
}

// ListUsers handles GET /users route
// @Summary List all users
// @Tags users
// @Accept json
// @Produce json
// @Param Authorization header string true "Access token"
// @Success 200 {array} dbmodels.User
// @Failure 401 {object} map[string]string
// @Failure 500 {object} map[string]string
// @Router /users [get]
func (h *Handler) ListUsers(c echo.Context) error {
    users := []dbmodels.User{}
    if err := h.DB.Find(&users).Error; err != nil {
        return c.JSON(http.StatusInternalServerError, map[string]string{"error": err.Error()})
    }

    // Loop through each user and get their roles and groups
    for i, user := range users {
        roles, groups, err := h.GetUserRolesAndGroups(user.ID)
        if err != nil {
            return c.JSON(http.StatusInternalServerError, map[string]string{"error": "failed to get user roles and groups"})
        }
        users[i].Roles = make([]*dbmodels.Role, len(roles))
        for j, r := range roles {
            users[i].Roles[j] = r
        }
        users[i].Groups = make([]*dbmodels.Group, len(groups))
        for j, g := range groups {
            users[i].Groups[j] = g
        }
    }

    return c.JSON(http.StatusOK, users)
}

func (h *Handler) GetUserRolesAndGroups(userID uint) ([]*dbmodels.Role, []*dbmodels.Group, error) {
    user := &dbmodels.User{}
    if err := h.DB.Preload("Roles").Preload("Groups").First(user, userID).Error; err != nil {
        if err == gorm.ErrRecordNotFound {
            return nil, nil, fmt.Errorf("user with id %d not found", userID)
        }
        return nil, nil, fmt.Errorf("failed to get user with id %d: %w", userID, err)
    }
    roles := make([]*dbmodels.Role, len(user.Roles))
    for i, r := range user.Roles {
        roles[i] = r
    }
    groups := make([]*dbmodels.Group, len(user.Groups))
    for i, g := range user.Groups {
        groups[i] = g
    }
    return roles, groups, nil
}

// GetUser gets a user by ID
// GetUser handles GET /users/:id route
// @Summary Get a user
// @Tags users
// @Accept json
// @Produce json
// @Param id path int true "User ID"
// @Param Authorization header string true "Access token"
// @Success 200 {object} dbmodels.User
// @Failure 400 {object} map[string]string
// @Failure 401 {object} map[string]string
// @Failure 404 {object} map[string]string
// @Failure 500 {object} map[string]string
// @Router /users/{id} [get]
func (h *Handler) GetUser(c echo.Context) error {
	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		return c.JSON(http.StatusBadRequest, map[string]string{"error": "invalid user ID"})
	}

	user := &dbmodels.User{}
	if err := h.DB.First(user, id).Error; err != nil {
		if err == gorm.ErrRecordNotFound {
			return c.JSON(http.StatusNotFound, map[string]string{"error": "user not found"})
		}
		return c.JSON(http.StatusInternalServerError, map[string]string{"error": err.Error()})
	}

	// Get user roles
	var roles []dbmodels.Role
	if err := h.DB.Model(user).Association("Roles").Find(&roles); err != nil {
		return c.JSON(http.StatusInternalServerError, map[string]string{"error": "failed to get user roles"})
	}

	// Get user groups
	var groups []dbmodels.Group
	if err := h.DB.Model(user).Association("Groups").Find(&groups); err != nil {
		return c.JSON(http.StatusInternalServerError, map[string]string{"error": "failed to get user groups"})
	}

	// Create a new response object that includes the user, roles, and groups
	response := map[string]interface{}{
		"user":   user,
		"roles":  roles,
		"groups": groups,
	}

	return c.JSON(http.StatusOK, response)
}

// CreateUser creates a new user
// CreateUser handles POST /users route
// @Summary Create a user
// @Tags users
// @Accept json
// @Produce json
// @Param Authorization header string true "Access token"
// @Param user body dtos.UserDto true "User object"
// @Success 201 {object} dbmodels.User
// @Failure 400 {object} map[string]string
// @Failure 500 {object} map[string]string
// @Router /users [post]
func (h *Handler) CreateUser(c echo.Context) error {

	userDto := &dtos.UserDto{}
	if err := c.Bind(userDto); err != nil {
		return c.JSON(http.StatusBadRequest, map[string]string{"error": err.Error()})
	}
    hashedPassword, err := bcrypt.GenerateFromPassword([]byte(userDto.Password), bcrypt.DefaultCost)
    if err != nil {
        return c.JSON(http.StatusInternalServerError, map[string]string{"error": err.Error()})
    }
	userInfo := &dbmodels.User{
		Name:      userDto.FullName,
		Email:     userDto.Email,
		Password:  string(hashedPassword),
		CreatedAt: time.Now(),
	}

	// convert role IDs to dbmodels.Role objects
	for _, roleID := range userDto.RoleIDs {
		role := &dbmodels.Role{}
		if err := h.DB.First(role, roleID).Error; err != nil {
			return c.JSON(http.StatusBadRequest, map[string]string{"error": err.Error()})
		}
		userInfo.Roles = append(userInfo.Roles, role)
	}

	// convert group IDs to dbmodels.Group objects
	for _, groupID := range userDto.GroupIDs {
		group := &dbmodels.Group{}
		if err := h.DB.First(group, groupID).Error; err != nil {
			return c.JSON(http.StatusBadRequest, map[string]string{"error": err.Error()})
		}
		userInfo.Groups = append(userInfo.Groups, group)
	}

	if err := h.DB.Create(userInfo).Error; err != nil {
		return c.JSON(http.StatusInternalServerError, map[string]string{"error": err.Error()})
	}

	return c.JSON(http.StatusCreated, userInfo)

}

// UpdateUser updates an existing user
// UpdateUser handles PUT /users/:id route
// @Summary Update a user
// @Tags users
// @Accept json
// @Produce json
// @Param id path int true "User ID"
// @Param Authorization header string true "Access token"
// @Param user body dbmodels.UpdateUser true "User object"
// @Success 200 {object} dtos.UserDto
// @Failure 400 {object} map[string]string
// @Failure 404 {object} map[string]string
// @Failure 500 {object} map[string]string
// @Router /users/{id} [put]
func (h *Handler) UpdateUser(c echo.Context) error {

    id, err := strconv.Atoi(c.Param("id"))
    if err != nil {
        return c.JSON(http.StatusBadRequest, map[string]string{"error": "invalid user ID"})
    }

    updateUser := &dbmodels.User{}
    if err := h.DB.First(updateUser, id).Error; err != nil {
        if err == gorm.ErrRecordNotFound {
            return c.JSON(http.StatusNotFound, map[string]string{"error": "user not found"})
        }
        return c.JSON(http.StatusInternalServerError, map[string]string{"error": err.Error()})
    }

    if err := c.Bind(updateUser); err != nil {
        return c.JSON(http.StatusBadRequest, map[string]string{"error": err.Error()})
    }

    // Update fields that are allowed to be updated
    updateUser.UpdatedAt = time.Now()

	// Hash the new password
	if updateUser.Password != "" {
        hashedPassword, err := bcrypt.GenerateFromPassword([]byte(updateUser.Password), bcrypt.DefaultCost)
        if err != nil {
            return c.JSON(http.StatusInternalServerError, map[string]string{"error": err.Error()})
        }
        updateUser.Password = string(hashedPassword)
    }

    if err := h.DB.Save(updateUser).Error; err != nil {
        return c.JSON(http.StatusInternalServerError, map[string]string{"error": err.Error()})
    }

    return c.JSON(http.StatusOK, updateUser)
}

// DeleteUser deletes a user by ID
// DeleteUser handles DELETE /users/:id route
// @Summary Delete a user
// @Tags users
// @Accept json
// @Produce json
// @Param id path int true "User ID"
// @Param Authorization header string true "Access token"
// @Security ApiKeyAuth
// @Success 204
// @Failure 400 {object} map[string]string
// @Failure 401 {object} map[string]string
// @Failure 404 {object} map[string]string
// @Failure 500 {object} map[string]string
// @Router /users/{id} [delete]
func (h *Handler) DeleteUser(c echo.Context) error {
	
	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		return c.JSON(http.StatusBadRequest, map[string]string{"error": "invalid user ID"})
	}
	user := &dbmodels.User{}
	if err := h.DB.First(user, id).Error; err != nil {
		if err == gorm.ErrRecordNotFound {
			return c.JSON(http.StatusNotFound, map[string]string{"error": "user not found"})
		}
		return c.JSON(http.StatusInternalServerError, map[string]string{"error": err.Error()})
	}
	tx := h.DB.Begin()
	// Delete the user's roles.
    if err := tx.Model(user).Association("Roles").Clear(); err != nil {
        tx.Rollback()
        return err
    }
    
    // Delete the user's groups.
    if err := tx.Model(user).Association("Groups").Clear(); err != nil {
        tx.Rollback()
        return err
    }


	if err := tx.Delete(user).Error; err != nil {
		return c.JSON(http.StatusInternalServerError, map[string]string{"error": err.Error()})
	}
	
	tx.Commit()

	// return c.NoContent(http.StatusNoContent)
	return c.JSON(http.StatusOK, map[string]string{
		"message": "User deleted successfully",
	})

}
