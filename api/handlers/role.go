package handlers

import (
	"net/http"
	"strconv"
	"PROJECTGO1/db/dbmodels"
	"PROJECTGO1/api/dtos"
	"github.com/labstack/echo/v4"
	"gorm.io/gorm"
	"time"
	
)


// NewRoleHandler creates a new RoleHandler with the given DB instance

func NewRoleHandler(db *gorm.DB) *Handler {
	return &Handler{DB: db}
}
// CreateRole
// @Summary Create a new role
// @Description Create a new role with the specified data
// @Tags roles
// @Accept  json
// @Produce  json
// @Param Authorization header string true "Access token"
// @Param role body dtos.NewRoleDto true "Role object"
// @Failure 400 {object} map[string]string
// @Failure 404 {object} map[string]string
// @Failure 500 {object} map[string]string
// @Router /roles [post]
func (h *Handler) CreateRole(c echo.Context) error {
	roleDto := &dtos.NewRoleDto{}
	if err := c.Bind(roleDto); err != nil {
		return c.JSON(http.StatusBadRequest, echo.Map{"error": err.Error()})
	}
	roleInfo := &dbmodels.Role{}
	roleInfo.CreatedAt = time.Now()
	roleInfo.Name = roleDto.Name
	roleInfo.Description = roleDto.Description


	result := h.DB.Create(roleInfo)
	if result.Error != nil {
		return c.JSON(http.StatusInternalServerError, echo.Map{"error": result.Error.Error()})
	}

	return c.JSON(http.StatusCreated, map[string]interface{}{
		"message": "Role created successfully",
		"role":    roleDto,
	})
}

// GetRole
// @Summary Get a role by ID
// @Description Get a role by ID
// @Tags roles
// @Accept  json
// @Produce  json
// @Param Authorization header string true "Access token"
// @Param id path int true "Role ID"
// @Success 200 {object} dbmodels.Role
// @Failure 400 {object} map[string]string
// @Failure 404 {object} map[string]string
// @Failure 500 {object} map[string]string
// @Router /roles/{id} [get]
func (h *Handler) GetRole(c echo.Context) error {
	roleID, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		return c.JSON(http.StatusBadRequest, echo.Map{"error": "invalid role ID"})
	}

	role := &dbmodels.Role{}
	result := h.DB.First(role, roleID)
	if result.Error != nil {
		if result.Error == gorm.ErrRecordNotFound {
			return c.JSON(http.StatusNotFound, echo.Map{"error": "role not found"})
		}
		return c.JSON(http.StatusInternalServerError, echo.Map{"error": result.Error.Error()})
	}

	return c.JSON(http.StatusOK, role)
}

// UpdateRole handles PUT /roles/:id to update an existing role
// UpdateRole
// @Summary Update a role by ID
// @Description Update a role by ID
// @Tags roles
// @Accept  json
// @Produce  json
// @Param Authorization header string true "Access token"
// @Param id path int true "Role ID"
// @Param role body dtos.UpdatedRoleDto true "Role object"
// @Success 200 {object} dbmodels.Role
// @Failure 400 {object} map[string]string
// @Failure 404 {object} map[string]string
// @Failure 500 {object} map[string]string
// @Router /roles/{id} [put]
func (h *Handler) UpdateRole(c echo.Context) error {
    roleID, err := strconv.Atoi(c.Param("id"))
    if err != nil {
        return c.JSON(http.StatusBadRequest, map[string]string{"error": "invalid role ID"})
    }

    roleDto := &dbmodels.Role{}
    if err := h.DB.First(roleDto, roleID).Error; err != nil {
        if err == gorm.ErrRecordNotFound {
            return c.JSON(http.StatusNotFound, map[string]string{"error": "role not found"})
        }
        return c.JSON(http.StatusInternalServerError, map[string]string{"error": err.Error()})
    }
    updateRoleDto := &dtos.UpdatedRoleDto{}
    if err := c.Bind(updateRoleDto); err != nil {
        return c.JSON(http.StatusBadRequest, map[string]string{"error": err.Error()})
    }

    roleDto.Name = updateRoleDto.Name
    roleDto.Description = updateRoleDto.Description
    if err := h.DB.Save(roleDto).Error; err != nil {
        return c.JSON(http.StatusInternalServerError, map[string]string{"error": err.Error()})
    }

    updatedRole := &dbmodels.Role{}
    updatedRole.CreatedAt = time.Now()
    updatedRole.Name = roleDto.Name
    updatedRole.Description = roleDto.Description
    return c.JSON(http.StatusOK, map[string]string{
        "message": "Role updated successfully",
    })
}

// DeleteRole handles DELETE /roles/:id to delete a role by its ID
// @Summary Delete a role
// @Description Delete a role by its ID
// @Tags roles
// @Accept json
// @Produce json
// @Param Authorization header string true "Access token"
// @Param id path int true "Role ID"
// @Success 204 "No Content"
// @Failure 400 {object} map[string]string
// @Failure 500 {object} map[string]string
// @Router /roles/{id} [delete]
func (h *Handler) DeleteRole(c echo.Context) error {
	roleID, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		return c.JSON(http.StatusBadRequest, echo.Map{"error": "invalid role ID"})
	}

	result := h.DB.Delete(&dbmodels.Role{}, roleID)
	if result.Error != nil {
		return c.JSON(http.StatusInternalServerError, echo.Map{"error": result.Error.Error()})
	}

	return c.JSON(http.StatusOK, map[string]string{
		"message": "Role deleted successfully",
	})
}

// ListRoles handles GET /roles to list all roles
// @Summary List all roles
// @Description List all roles
// @Tags roles
// @Accept json
// @Produce json
// @Param Authorization header string true "Access token"
// @Success 200 {array} dbmodels.Role
// @Failure 500 {object} map[string]string
// @Router /roles [get]
func (h *Handler) ListRoles(c echo.Context) error {
    roles := []*dbmodels.Role{}
    result := h.DB.Find(&roles)
    if result.Error != nil {
        return c.JSON(http.StatusInternalServerError, echo.Map{"error": result.Error.Error()})
    }

    return c.JSON(http.StatusOK, roles)
}