package handlers

import (
	"PROJECTGO1/config"
	"fmt"
	"net/http"
	"time"

	"github.com/dgrijalva/jwt-go"
	"github.com/labstack/echo/v4"
)



type RefreshTokenResponse struct {
    Token       string `json:"token"`
    ExpiresAt   int64  `json:"expires_at"`
}
// RefreshToken refreshes an existing JWT token
// @Summary Refresh JWT token
// @Description Refreshes an existing JWT token and returns a new token with an updated expiration time
// @Accept  json
// @Produce  json
// @Security ApiKeyAuth
// @Success 200 {object} RefreshTokenResponse
// @Failure 401 {string} string "Unauthorized"
// @Failure 500 {string} string "Internal server error"
// @Router /auth/refresh_token [post]
func RefreshToken(c echo.Context) error {
    accessToken := c.FormValue("access_token")
    if accessToken == "" {
        fmt.Println("No access token provided")
        return echo.ErrUnauthorized
    }

    // Parse the access token claims without validating
    token, err := jwt.ParseWithClaims(
        accessToken,
        &JWTClaim{},
        func(token *jwt.Token) (interface{}, error) {
            return []byte(config.JwtSecretKey), nil
        },
    )
    if err != nil {
        fmt.Println("Error parsing token:", err)
        return echo.ErrUnauthorized
    }

    // Extract the isAdmin value from the access token's claims
    claims, ok := token.Claims.(*JWTClaim)
    if !ok {
        return echo.ErrUnauthorized
    }
    isAdmin := claims.IsAdmin

    // Validate the access token using the existing ValidateToken function
    err, isAdmin = ValidateToken(accessToken)
    if err != nil {
        fmt.Println("Error validating token:", err)
        return echo.ErrUnauthorized
    }

    // Create a new token object
    expirationTime := time.Now().Add(24 * time.Hour)
    newClaims := &JWTClaim{
        UserId: claims.UserId,
        IsAdmin: isAdmin,
        StandardClaims: jwt.StandardClaims{
            ExpiresAt: expirationTime.Unix(),
        },
    }
    newToken := jwt.NewWithClaims(jwt.SigningMethodHS256, newClaims)

    // Generate the signed token string
    newTokenString, err := newToken.SignedString([]byte(config.JwtSecretKey))
    if err != nil {
        fmt.Println("Error generating signed token string:", err)
        return err
    }

    return c.JSON(http.StatusOK, map[string]string{"token": newTokenString})
}




