package routes

import (
	"PROJECTGO1/api/handlers"
	"github.com/labstack/echo/v4"
)

func RegisterUserRoutes(e *echo.Echo, h *handlers.Handler) {
	usersGroup := e.Group("/users")
	createUserHandler := h.CreateUser
	// Create user route
	// @Summary Create a new user
	// @Description Create a new user in the system
	// @Tags users
	// @Accept json
	// @Produce json
	// @Param user body User true "User object"
	// @Security bearerAuth
	// @Router /users [post]
	usersGroup.POST("", handlers.CheckAdmin()(createUserHandler))
	deleteUserHandler := h.DeleteUser
	// Delete user by ID route
	// @Summary Delete a user by ID
	// @Description Delete a user by its ID
	// @Tags users
	// @Produce json
	// @Param id path int true "User ID"
	// @Security bearerAuth
	// @Router /users/{id} [delete]
	usersGroup.DELETE("/:id", handlers.CheckAdmin()(deleteUserHandler))
	getUserHandler := h.GetUser
	// @Summary Get a user by ID
	// @Description Get a user by its ID
	// @Tags users
	// @Produce json
	// @Param id path int true "User ID"
	// @Security bearerAuth
	// @Router /users/{id} [get]
	usersGroup.GET("/:id", handlers.CheckAdmin()(getUserHandler))
	listUserHandler := h.ListUsers
	// @Summary List all users
	// @Description List all users in the system
	// @Tags users
	// @Produce json
	// @Success 200 {array} User
	// @Security bearerAuth
	// @Router /users [get]
	usersGroup.GET("", handlers.CheckAdmin()(listUserHandler))
	updateUserHandler := h.UpdateUser
	// @Summary Update a user by ID
	// @Description Update an existing user by its ID
	// @Tags users
	// @Accept json
	// @Produce json
	// @Param id path int true "User ID"
	// @Param user body User true "Updated user object"
	// @Success 200 {object} User
	// @Security bearerAuth
	// @SecurityRequirement bearerAuth
	// @Router /users/{id} [put]
	usersGroup.PUT("/:id", handlers.CheckAdmin()(updateUserHandler))
}

