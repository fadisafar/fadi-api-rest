package routes

import (
	"PROJECTGO1/api/handlers"

	"github.com/labstack/echo/v4"

)

func RegisterRoleRoutes(e *echo.Echo, h *handlers.Handler) {
    // List all roles
    // @Summary List all roles
    // @Description List all roles in the system
    // @Tags roles
    // @Produce json
    // @Success 200 {array} Role
    // @Router /roles [get]
    e.GET("/roles", handlers.CheckAdmin()(h.ListRoles))
    // Get a role by ID
    // @Summary Get a role by ID
    // @Description Get a role by its ID
    // @Tags roles
    // @Produce json
    // @Param id path int true "Role ID"
    // @Success 200 {object} Role
    // @Router /roles/{id} [get]
    e.GET("/roles/:id", handlers.CheckAdmin()(h.GetRole))
    // Create a new role
    // @Summary Create a new role
    // @Description Create a new role in the system
    // @Tags roles
    // @Accept json
    // @Produce json
    // @Param role body Role true "Role object"
    // @Success 201 {object} Role
    // @Router /roles [post]
    e.POST("/roles", handlers.CheckAdmin()(h.CreateRole))
    // Update a role by ID
    // @Summary Update a role by ID
    // @Description Update an existing role by its ID
    // @Tags roles
    // @Accept json
    // @Produce json
    // @Param id path int true "Role ID"
    // @Param role body Role true "Updated role object"
    // @Success 200 {object} Role
    // @Router /roles/{id} [put]
    e.PUT("/roles/:id", handlers.CheckAdmin()(h.UpdateRole))
    // Delete a role by ID
    // @Summary Delete a role by ID
    // @Description Delete a role by its ID
    // @Tags roles
    // @Produce json
    // @Param id path int true "Role ID"
    // @Success 204 "No content"
    // @Router /roles/{id} [delete]
    e.DELETE("/roles/:id", handlers.CheckAdmin()(h.DeleteRole))
}

