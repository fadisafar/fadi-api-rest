package routes

import (
	"PROJECTGO1/api/handlers"

	"github.com/labstack/echo/v4"
	"gorm.io/gorm"
)


func RegisterAuthRoutes(e *echo.Echo,h *handlers.AuthHandler, dbc *gorm.DB) {
    e.POST("/auth/login", func(c echo.Context) error {
        return handlers.Login(c, dbc)
    })
}