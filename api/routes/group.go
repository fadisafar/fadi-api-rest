package routes

import (
    // "github.com/swaggo/echo-swagger"
    "PROJECTGO1/api/handlers"
    "github.com/labstack/echo/v4"
)

func RegisterGroupRoutes(e *echo.Echo, h *handlers.Handler) {

    // GetGroup - get group by ID
    // @Summary Get a group by ID
    // @Description Get a group by its ID
    // @Tags groups
    // @Produce json
    // @Param id path int true "Group ID"
    // @Success 200 {object} Group
    // @Router /groups/{id} [get]
    e.GET("/groups/:id", handlers.CheckAdmin()(h.GetGroup))
    //@Summary List all groups
    // @Description List all groups in the system
    // @Tags groups
    // @Produce json
    // @Success 200 {array} Group
    // @Router /groups [get]
    e.GET("/groups", handlers.CheckAdmin()(h.GetGroups))
    // CreateGroup - create a new group
    // @Summary Create a new group
    // @Description Create a new group in the system
    // @Tags groups
    // @Accept json
    // @Produce json
    // @Param group body Group true "Group object"
    // @Success 201 {object} Group
    // @Router /groups [post]
    e.POST("/groups", handlers.CheckAdmin()(h.CreateGroup))
    // UpdateGroup - update a group by ID
    // @Summary Update a group by ID
    // @Description Update an existing group by its ID
    // @Tags groups
    // @Accept json
    // @Produce json
    // @Param id path int true "Group ID"
    // @Param group body Group true "Updated group object"
    // @Success 200 {object} Group
    // @Router /groups/{id} [put]
    e.PUT("/groups/:id", handlers.CheckAdmin()(h.UpdateGroup))
    // DeleteGroup - delete a group by ID
    // @Summary Delete a group by ID
    // @Description Delete a group by its ID
    // @Tags groups
    // @Produce json
    // @Param id path int true "Group ID"
    // @Success 204 "No content"
    // @Router /groups/{id} [delete]
    e.DELETE("/groups/:id", handlers.CheckAdmin()(h.DeleteGroup))
}
