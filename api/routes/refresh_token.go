package routes

import (
	"PROJECTGO1/api/handlers"
	"github.com/labstack/echo/v4"
)

// @Summary Refresh JWT access token
// @Description Endpoint to refresh a JWT access token
// @Tags authentication
// @Accept json
// @Produce json
// @Param refreshToken body string true "Refresh token"
// @Success 200 {object} handlers.RefreshTokenResponse
// @Failure 401 {object} handlers.RefreshTokenResponse
// @Failure 500 {object} handlers.RefreshTokenResponse
// @Router /auth/refresh_token [post]
func RegisterRefreshTokenRoutes(e *echo.Echo) {
    e.POST("/auth/refresh_token", handlers.RefreshToken)
}

