module PROJECTGO1

go 1.20

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible // direct
	github.com/labstack/echo/v4 v4.10.2 // direct
	golang.org/x/crypto v0.7.0 // direct
	gorm.io/driver/postgres v1.5.0 // direct
	gorm.io/gorm v1.24.7-0.20230306060331-85eaf9eeda11 // direct
)

require (
	github.com/KyleBanks/depth v1.2.1 // indirect
	github.com/go-openapi/jsonpointer v0.19.6 // indirect
	github.com/go-openapi/jsonreference v0.20.2 // indirect
	github.com/go-openapi/spec v0.20.8 // indirect
	github.com/go-openapi/swag v0.22.3 // indirect
	github.com/jackc/pgpassfile v1.0.0 // indirect
	github.com/jackc/pgservicefile v0.0.0-20221227161230-091c0ba34f0a // indirect
	github.com/jackc/pgx/v5 v5.3.1 // indirect
	github.com/jinzhu/inflection v1.0.0 // indirect
	github.com/jinzhu/now v1.1.5 // indirect
	github.com/josharian/intern v1.0.0 // indirect
	github.com/kr/pretty v0.3.1 // indirect
	github.com/labstack/gommon v0.4.0 // indirect
	github.com/mailru/easyjson v0.7.7 // indirect
	github.com/mattn/go-colorable v0.1.13 // indirect
	github.com/mattn/go-isatty v0.0.18 // indirect
	github.com/rogpeppe/go-internal v1.10.0 // indirect
	github.com/swaggo/files/v2 v2.0.0 // indirect
	github.com/valyala/bytebufferpool v1.0.0 // indirect
	github.com/valyala/fasttemplate v1.2.2 // indirect
	golang.org/x/net v0.8.0 // indirect
	golang.org/x/sys v0.6.0 // indirect
	golang.org/x/text v0.8.0 // indirect
	golang.org/x/tools v0.7.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)

require (
	github.com/swaggo/echo-swagger v1.4.0
	github.com/swaggo/swag v1.8.12
	gorm.io/plugin/dbresolver v1.4.1
)

require (
	github.com/golang-jwt/jwt v3.2.2+incompatible // indirect; direct
	github.com/joho/godotenv v1.5.1 // direct
	golang.org/x/time v0.3.0 // indirect
)
